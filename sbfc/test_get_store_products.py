from fastapi.testclient import TestClient
from main import app
from queries.products import ProductQueries

client = TestClient(app)


class GetStoreProductsQueries:
    def get_store_products(self, store_id):
        return []


def test_get_store_products():
    # arrange
    app.dependency_overrides[ProductQueries] = GetStoreProductsQueries

    # act
    response = client.get("/api/stores/2020/products")

    # cleanup
    app.dependency_overrides = {}

    # assert
    assert response.status_code == 200
    assert response.json() == {"products": []}
