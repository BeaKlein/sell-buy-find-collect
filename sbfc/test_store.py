from fastapi.testclient import TestClient
from main import app
from queries.stores import StoreQueries

client = TestClient(app)


class EmptyStoreQueries:
    def get_all_stores(self):
        return []


def test_get_all_stores():
    # arrange
    app.dependency_overrides[StoreQueries] = EmptyStoreQueries

    # act
    response = client.get("/api/stores")

    # cleanup
    app.dependency_overrides = {}

    # assert
    assert response.status_code == 200
    assert response.json() == {"stores": []}
