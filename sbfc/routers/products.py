from fastapi import APIRouter, Depends, HTTPException, status, Response
from models.products import (
    ProductIn,
    ProductOut,
    ProductAll,
    ProductOutUpdate,
    ProductUpdate,
)
from queries.products import ProductQueries
from queries.stores import StoreQueries
from authenticator import authenticator

router = APIRouter()

not_authorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Invalid authentication credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


@router.get("/api/products/{product_id}", response_model=ProductOut)
def get_product(product_id, repo: ProductQueries = Depends()):
    product = repo.get_product(product_id)
    return product


@router.post("/api/products", response_model=ProductOut)
async def create_product(
    product: ProductIn,
    repo: ProductQueries = Depends(),
    store_repo: StoreQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    new_product = repo.create_product(product)
    store_repo.append_products(product.store_id, new_product.id)
    return new_product


@router.get("/api/products", response_model=ProductAll)
def get_all_products(repo: ProductQueries = Depends()):
    return ProductAll(products=repo.get_all_products())


@router.get("/api/stores/{store_id}/products", response_model=ProductAll)
def get_store_products(store_id: str, repo: ProductQueries = Depends()):
    return ProductAll(products=repo.get_store_products(store_id))


@router.get("/api/cart/{cart_id}/products", response_model=ProductAll)
def get_cart_products(cart_id: str, repo: ProductQueries = Depends()):
    return ProductAll(products=repo.get_cart_products(cart_id))


@router.delete("/api/products/{product_id}", response_model=bool)
async def delete_product(
    product_id: str,
    repo: ProductQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    repo.delete_product(product_id)
    return True


@router.put("/api/products/{product_id}", response_model=ProductOutUpdate)
async def update_product(
    product_id: str,
    product: ProductUpdate,
    response: Response,
    repo: ProductQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    product = repo.update_product(product_id, product)
    return product
