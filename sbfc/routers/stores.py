from fastapi import APIRouter, Depends, HTTPException, status, Response
from models.stores import (
    StoreIn,
    StoreOut,
    StoreAll,
    StoreUpdate,
    StoreOutUpdated,
)
from queries.stores import StoreQueries
from queries.accounts import AccountQueries
from authenticator import authenticator

router = APIRouter()

not_authorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Invalid authentication credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


@router.get("/api/stores/{store_id}", response_model=StoreOut)
def get_store(store_id, repo: StoreQueries = Depends()):
    store = repo.get_store(store_id)
    return store


@router.post("/api/stores", response_model=StoreOut)
async def create_store(
    store: StoreIn,
    repo: StoreQueries = Depends(),
    account_repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    new_store = repo.create_store(store)
    account_repo.assign_store(store.account_id, new_store.id)
    return new_store


@router.get("/api/stores", response_model=StoreAll)
def get_all_stores(repo: StoreQueries = Depends()):
    return StoreAll(stores=repo.get_all_stores())


@router.delete("/api/stores/{store_id}", response_model=bool)
async def delete_store(
    store_id: str,
    repo: StoreQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    repo.delete_store(store_id)
    return True


@router.put("/api/stores/{store_id}", response_model=StoreOutUpdated)
async def update_store(
    store_id: str,
    store: StoreUpdate,
    response: Response,
    repo: StoreQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    store = repo.update_store(store_id, store)
    return store


@router.put(
    "/api/stores/{store_id}/add_product/{product_id}", response_model=StoreOut
)
async def append_products(
    store_id: str,
    product_id: str,
    response: Response,
    repo: StoreQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    store = repo.append_products(store_id, product_id)
    return store


@router.put(
    "/api/stores/{store_id}/remove_product/{product_id}",
    response_model=StoreOut,
)
async def remove_products(
    store_id: str,
    product_id: str,
    response: Response,
    repo: StoreQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    store = repo.remove_products(store_id, product_id)
    return store
