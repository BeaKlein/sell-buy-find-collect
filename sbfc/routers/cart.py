from fastapi import APIRouter, Depends, HTTPException, status, Response
from models.cart import CartIn, CartOut, CartAll
from queries.cart import CartQueries
from queries.accounts import AccountQueries
from authenticator import authenticator

router = APIRouter()

not_authorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Invalid authentication credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


@router.get("/api/cart/{cart_id}", response_model=CartOut)
def get_cart(cart_id, repo: CartQueries = Depends()):
    cart = repo.get_cart(cart_id)
    return cart


@router.post("/api/cart", response_model=CartOut)
async def create_cart(
    cart: CartIn,
    repo: CartQueries = Depends(),
    account_repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    new_cart = repo.create_cart(cart)
    account_repo.assign_cart(cart.account_id, new_cart.id)
    return new_cart


@router.get("/api/carts", response_model=CartAll)
def get_all_carts(repo: CartQueries = Depends()):
    return CartAll(carts=repo.get_all_carts())


@router.delete("/api/cart/{cart_id}", response_model=bool)
async def delete_cart(
    cart_id: str,
    account_id: str,
    repo: CartQueries = Depends(),
    account_repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    repo.delete_cart(cart_id)
    account_repo.remove_cart(account_id)
    return True


@router.put("/api/cart/{cart_id}", response_model=CartOut)
async def update_cart(
    cart_id: str,
    cart: CartIn,
    response: Response,
    repo: CartQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    cart = repo.update_cart(cart_id, cart)
    return cart


@router.put(
    "/api/cart/{cart_id}/add_product/{product_id}/{quantity}",
    response_model=CartOut,
)
async def add_product_to_cart(
    cart_id: str,
    product_id: str,
    quantity: int,
    response: Response,
    repo: CartQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    updated_cart = repo.add_product_to_cart(cart_id, product_id, quantity)
    return updated_cart


@router.put(
    "/api/cart/{cart_id}/remove_product/{product_id}", response_model=CartOut
)
async def remove_product_from_cart(
    cart_id: str,
    product_id: str,
    response: Response,
    repo: CartQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    cart = repo.remove_product_from_cart(cart_id, product_id)
    return cart


@router.put("/api/cart/{cart_id}/clear_cart", response_model=CartOut)
async def clear_cart(
    cart_id: str,
    response: Response,
    repo: CartQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    cart = repo.clear_cart(cart_id)
    return cart
