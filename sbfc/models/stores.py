from pydantic import BaseModel
from bson.objectid import ObjectId


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except ValueError:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class StoreIn(BaseModel):
    store_name: str
    description: str
    img_url: str
    account_id: str
    products: list = []


class StoreUpdate(BaseModel):
    store_name: str
    description: str
    img_url: str


class Store(StoreIn):
    id: PydanticObjectId


class StoreOutUpdated(StoreUpdate):
    id: str


class StoreOut(StoreIn):
    id: str


class StoreAll(BaseModel):
    stores: list
