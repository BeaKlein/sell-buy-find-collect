from pydantic import BaseModel
from bson.objectid import ObjectId


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except ValueError:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class ProductIn(BaseModel):
    product_name: str
    product_description: str
    product_img_url: str
    product_price: float
    store_id: str


class ProductUpdate(BaseModel):
    product_name: str
    product_description: str
    product_img_url: str
    product_price: float


class Product(ProductIn):
    id: PydanticObjectId


class ProductOutUpdate(ProductUpdate):
    id: str


class ProductOut(ProductIn):
    id: str


class ProductAll(BaseModel):
    products: list
