from bson.objectid import ObjectId
from .client import Queries
from models.stores import (
    StoreIn,
    StoreOut,
    StoreAll,
    StoreUpdate,
    StoreOutUpdated,
)
from pymongo import ReturnDocument


class StoreQueries(Queries):
    DB_NAME = "mongodata"
    COLLECTION = "stores"

    def get_store(self, store_id: str) -> StoreOut:
        props = self.collection.find_one({"_id": ObjectId(store_id)})
        if not props:
            return None
        props["id"] = str(props["_id"])
        return StoreOut(**props)

    def create_store(self, store: StoreIn) -> StoreOut:
        props = store.dict()
        self.collection.insert_one(props)
        props["id"] = str(props["_id"])
        return StoreOut(**props)

    def get_all_stores(self) -> StoreAll:
        db = self.collection.find()
        stores = []
        for document in db:
            document["id"] = str(document["_id"])
            stores.append(StoreOut(**document))
        return stores

    def delete_store(self, store_id: str) -> bool:
        return self.collection.delete_one({"_id": ObjectId(store_id)})

    def update_store(
        self, store_id: str, store: StoreUpdate
    ) -> StoreOutUpdated:
        props = store.dict()
        self.collection.find_one_and_update(
            {"_id": ObjectId(store_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return StoreOutUpdated(**props, id=store_id)

    def append_products(self, store_id: str, product_id: str) -> StoreOut:
        props = self.collection.find_one({"_id": ObjectId(store_id)})
        product_list = props["products"]
        product_list.append(product_id)
        self.collection.find_one_and_update(
            {"_id": ObjectId(store_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return StoreOut(**props, id=store_id)

    def remove_products(self, store_id: str, product_id: str) -> StoreOut:
        props = self.collection.find_one({"_id": ObjectId(store_id)})
        product_list = props["products"]
        if product_id:
            product_list.remove(product_id)
        else:
            return "Product does not exist"
        self.collection.find_one_and_update(
            {"_id": ObjectId(store_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return StoreOut(**props, id=store_id)
