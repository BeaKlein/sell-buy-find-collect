from bson.objectid import ObjectId
from .client import Queries
from models.products import (
    ProductIn,
    ProductOut,
    ProductAll,
    ProductUpdate,
    ProductOutUpdate,
)
from pymongo import ReturnDocument, MongoClient
import os


class ProductQueries(Queries):
    DB_NAME = "mongodata"
    COLLECTION = "products"

    def get_product(self, product_id: str) -> ProductOut:
        props = self.collection.find_one({"_id": ObjectId(product_id)})
        if not props:
            return None
        props["id"] = str(props["_id"])
        return ProductOut(**props)

    def create_product(self, product: ProductIn) -> ProductOut:
        props = product.dict()
        self.collection.insert_one(props)
        props["id"] = str(props["_id"])
        new_product = ProductOut(**props)
        return new_product

    def get_all_products(self) -> ProductAll:
        db = self.collection.find()
        products = []
        for document in db:
            document["id"] = str(document["_id"])
            products.append(ProductOut(**document))
        return products

    def get_store_products(self, store_id: str) -> ProductAll:
        DATABASE_URL = os.environ["DATABASE_URL"]
        conn = MongoClient(DATABASE_URL)
        db = conn.mongodata.stores
        store = db.find_one({"_id": ObjectId(store_id)})
        store_products = store["products"]
        store_product_list = []
        for product in store_products:
            props = self.collection.find_one({"_id": ObjectId(product)})
            if not props:
                return None
            props["id"] = str(props["_id"])
            store_product = ProductOut(**props)
            store_product_list.append(store_product)
        return store_product_list

    def delete_product(self, product_id: str) -> bool:
        return self.collection.delete_one({"_id": ObjectId(product_id)})

    def update_product(
        self, product_id: str, product: ProductUpdate
    ) -> ProductOutUpdate:
        props = product.dict()
        self.collection.find_one_and_update(
            {"_id": ObjectId(product_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return ProductOutUpdate(**props, id=product_id)

    def get_cart_products(self, cart_id: str) -> ProductAll:
        DATABASE_URL = os.environ["DATABASE_URL"]
        conn = MongoClient(DATABASE_URL)
        db = conn.mongodata.carts
        cart = db.find_one({"_id": ObjectId(cart_id)})
        cart_products = cart["cart_products"]
        cart_product_list = []
        for product in cart_products:
            props = self.collection.find_one(
                {"_id": ObjectId(product["product_id"])}
            )
            if not props:
                return None
            props["id"] = str(props["_id"])
            cart_product = ProductOut(**props)
            cart_product_dict = cart_product.dict()
            cart_product_dict["quantity"] = product["quantity"]
            store_id = props.get("store_id")
            DATABASE_URL = os.environ["DATABASE_URL"]
            conn = MongoClient(DATABASE_URL)
            storedb = conn.mongodata.stores
            store = storedb.find_one({"_id": ObjectId(store_id)})
            store_name = store.get("store_name")
            cart_product_dict["store_name"] = store_name
            cart_product_list.append(cart_product_dict)
        return cart_product_list
