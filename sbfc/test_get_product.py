from fastapi.testclient import TestClient
from main import app
from models.products import ProductOut
from queries.products import ProductQueries

client = TestClient(app)


class GetProductQueries:
    def get_product(self, product_id):
        return {
            "id": 3030,
            "product_name": "Donut Donut",
            "product_description": "Greatest Donut",
            "product_img_url": "google.com",
            "product_price": 100,
            "store_id": "donutstoreid",
        }


def test_get_store():
    # arrange
    app.dependency_overrides[ProductQueries] = GetProductQueries

    productout = ProductOut(
        id=3030,
        product_name="Donut Donut",
        product_description="Greatest Donut",
        product_img_url="google.com",
        product_price=100,
        store_id="donutstoreid",
    )

    # act
    response = client.get("/api/products/3030")

    # cleanup
    app.dependency_overrides = {}

    # assert
    assert response.status_code == 200
    assert response.json() == productout.dict()
