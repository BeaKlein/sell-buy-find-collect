import React, { useState, useEffect } from "react";
import { useCreateStoreMutation, useGetTokenQuery } from "./store/api";
import { useNavigate } from "react-router-dom";


function CreateStoreForm() {
  const [store_name, setStoreName] = useState("");
  const [description, setDescription] = useState("");
  const [img_url, setImgUrl] = useState("");
  const [createStore, result] = useCreateStoreMutation();

  const { data: tokenData } = useGetTokenQuery();

  const handleStoreNameChange = (event) => {
    const value = event.target.value;
    setStoreName(value);
  };

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handleImgUrlChange = (event) => {
    const value = event.target.value;
    setImgUrl(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    createStore({
      store_name,
      description,
      img_url,
      account_id: tokenData.account.id,
    });
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (result.isSuccess) {
      navigate("../../products/new");
    }
  });

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Store</h1>
          <form onSubmit={handleSubmit} id="create-store-form">
            <div className="form-floating mb-3">
              <input
                value={store_name}
                onChange={handleStoreNameChange}
                placeholder="Store Name"
                required
                type="text"
                id="store_name"
                className="form-control"
              />
              <label htmlFor="store_name">Store Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={description}
                onChange={handleDescriptionChange}
                placeholder="Description"
                required
                type="text"
                id="description"
                className="form-control"
              />
              <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={img_url}
                onChange={handleImgUrlChange}
                placeholder="Image Url"
                type="url"
                id="img_url"
                className="form-control"
              />
              <label htmlFor="img_url">Image Url</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateStoreForm;
