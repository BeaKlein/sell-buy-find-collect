import { useGetStoresQuery } from "./store/api";
import { Link } from "react-router-dom";


const linkStyle = {
  color: "black"
};


function ListStores() {
  const { data } = useGetStoresQuery();
  return (
    <>
      <h1 className="title text-center">All Stores</h1>
      <div className="text-center italic">~~~~~</div>
        <div className="spacing text-center">
        <div className="container">
          <div className="row">
          {data?.stores.map( store => {
            return (
                <div className="col-sm-4 shadow-lg p-3 mb-3 bg-white rounded" key={store.id}>
                  <div className="mb-3" style={linkStyle}><Link to={`/stores/${store.id}`}>{store.store_name}</Link></div>
                  <div><img src={store.img_url} className="img-float rounded" alt="store" width="300" height="300"/></div>
                  <div className="mt-3">{store.description}</div>
                </div>
            );
          })}
        </div>
        </div>
      </div>
    </>
  );
}

export default ListStores;
