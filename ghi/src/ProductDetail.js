import React, { useState, useEffect } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import {
  useRemoveProductFromStoreMutation,
  useGetProductQuery,
  useCreateCartMutation,
  useLazyGetStoreQuery,
  useAddProductToCartMutation,
  useGetTokenQuery,
} from "./store/api";
import { CiSquarePlus, CiSquareMinus } from 'react-icons/ci';

function AddToCart() {
  const { product_id } = useParams();
  const { data: tokenData } = useGetTokenQuery();
  const [addProductToCart] = useAddProductToCartMutation();
  const [createCart, cart_result] = useCreateCartMutation();
  const [counter, setCounter] = useState(1);
  const incrementCounter = () => setCounter(counter + 1);
  let decrementCounter = () => setCounter(counter - 1);
  if (counter <= 0) {
    decrementCounter = () => setCounter(1);
  }

  const data = {
    cart_id: tokenData?.account?.cart_id,
    product_id,
    quantity: counter,
  };
  const cart_id = data?.cart_id;
  const cart_data = {
    cart_products: [],
    total_cost: 0,
    account_id: tokenData?.account?.id,
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (cart_id === null) {
      createCart(cart_data);
      return;
    }
    addProductToCart(data);
  };

  useEffect(() => {
    if (cart_result?.data?.id) {
      addProductToCart({
        product_id,
        cart_id: cart_result.data.id,
        quantity: counter,
      });
    }
    // eslint-disable-next-line
  }, [cart_result]);

  return (
    <>
      <div>
        <CiSquarePlus
          size={23}
          className="mb-2 mt-2 mr-2"
          onClick={incrementCounter}
        />
        <display message={counter}> {counter} </display>
        <CiSquareMinus
          size={23}
          className="mb-2 mt-2 ml-2"
          onClick={decrementCounter}
        />
      </div>
      <div>
        <button className="mt-1 mb-3 bg-white rounded" onClick={handleSubmit}>
          Add to Cart
        </button>
      </div>
    </>
  );
}

function ProductDetail() {
  const { product_id } = useParams();
  const { data } = useGetProductQuery(product_id);
  const { data: tokenData } = useGetTokenQuery();
  const [trigger, result] = useLazyGetStoreQuery();
  const [removeProduct, removeResult] = useRemoveProductFromStoreMutation();

  useEffect(() => {
    if (data?.store_id) {
      trigger(data?.store_id);
    }
  // eslint-disable-next-line
  },[]);

  const navigate = useNavigate();
  useEffect(() => {
    if (removeResult.isSuccess) {
      navigate(`/stores/${tokenData?.account?.store_id}`);
    }
  });

  return (
    <>
      <h1 className="title">{data?.product_name}</h1>
      <h6>{result?.data?.store_name}</h6>
        <div className="container">
          <div className="row">
            <div className="col-1"></div>
            <div className="col-8">
              <div className="mb-4 mt-4" key={data?.id}>
                  <img
                    src={data?.product_img_url}
                    alt="product"
                    width="500"
                    height="500"
                  />
              </div>
              <div>
                {tokenData?.account?.store_id === data?.store_id ? (
                  <button className="mt-4 mb-2 btn-sm bg-white rounded">
                    <Link to={`edit`}>Edit Product</Link>
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div>
                {tokenData?.account?.store_id === data?.store_id ? (
                  <button
                    onClick={() =>
                      removeProduct({ store_id: data?.store_id, product_id })
                    }
                    className="btn-sm bg-white rounded"
                  >
                    Remove Product
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
            <span className="col-3 border border-dark bg-light">
              <div key={data?.id}>
                <h3 className="mt-2">{data?.product_name}</h3>
                <h4 className="mt-3 mb-3">${data?.product_price}</h4>
                <div className="mb-2"><AddToCart /></div>
                <div className="mb-2">Product Description:</div>
                <div className="mb-5">{data?.product_description}</div>
              </div>
            </span>
          </div>
        </div>
    </>
  );
}

export default ProductDetail;
