import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSignUpMutation } from './store/api';


function SignUpForm() {
  const navigate = useNavigate();
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [signUp, result] = useSignUpMutation();

  const handleUserNameChange = (event) => {
    const value = event.target.value;
    setUserName(value);
  };

  const handlePasswordChange = (event) => {
    const value = event.target.value;
    setPassword(value);
  };

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    signUp({ username, password, email });
  };

  useEffect(() => {
    if (result.isSuccess) {
      navigate("/");
    }
  });

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <div className="box content">
          <h3>Sign Up</h3>
          <form method="POST" onSubmit={handleSubmit} id="login-form">
            <div className="field">
              <label className="label" htmlFor="username">Username</label>
              <div className="control">
                <input required onChange={handleUserNameChange} value={username} name="username" className="input" type="text" placeholder="Username" id="username" />
              </div>
            </div>
            <div className="field">
              <label className="label">Email</label>
              <div className="control">
                <input required onChange={handleEmailChange} value={email} name="email" className="input" type="email" placeholder="Email" id="email" />
              </div>
            </div>
            <div className="field">
              <label className="label">Password</label>
              <div className="control">
                <input required onChange={handlePasswordChange} value={password} name="password" className="input" type="password" placeholder="Password" id="password" />
              </div>
            </div>
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
  );
}

export default SignUpForm;
