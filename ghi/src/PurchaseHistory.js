import {
  useGetTokenQuery,
  useGetUserPurchasesQuery,
} from "./store/api";
import { Link } from "react-router-dom";


function PurchaseHistory() {
  const { data: tokenData } = useGetTokenQuery();
  const account_id = tokenData?.account?.id;
  const { data: userPurchasesData } = useGetUserPurchasesQuery(account_id);

  return (
    <>
      <h1>Purchase History</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Order</th>
            <th>Number of Products</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {userPurchasesData?.carts?.map((purchase) => {
            return (
              <tr key={purchase.id}>
                <td>
                  <Link to={`${purchase.id}`}>{purchase.id}</Link>
                </td>
                <td>{purchase.cart_products.length}</td>
                <td>${purchase.total_cost}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default PurchaseHistory;
