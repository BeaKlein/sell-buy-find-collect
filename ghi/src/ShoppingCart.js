import { useParams, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import {
  useGetTokenQuery,
  useCheckoutCartMutation,
  useGetCartProductsQuery,
  useClearCartMutation,
  useRemoveProductFromCartMutation,
  useEditCartMutation,
  useGetCartQuery,
} from "./store/api";


function ShoppingCart() {
  const { cart_id } = useParams();
  const { data: cart_product_data } = useGetCartProductsQuery(cart_id);
  const { data: cart_data } = useGetCartQuery(cart_id);
  const [ clearCart ] = useClearCartMutation(cart_id);
  const [ removeProduct ] = useRemoveProductFromCartMutation();
  const [ checkOut, result ] = useCheckoutCartMutation();
  const [ editCart ] = useEditCartMutation();
  const { data: tokenData } = useGetTokenQuery();
  const account_id = tokenData?.account?.id;
  const navigate = useNavigate();

  useEffect(() => {
    if (result.isSuccess) {
      navigate(`/accounts/${account_id}/purchases/${cart_id}`);
    }
  });

  let total = 0;
  if (cart_product_data?.products) {
    for (let product of cart_product_data?.products) {
      total += product.product_price * product.quantity;
    }
  };

  let data = {
    cart_id: cart_id,
    data: {
      cart_products: cart_data?.cart_products,
      total_cost: total,
      account_id: account_id
    }
  }

  return (
    <>
      <h1>My Cart</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Store Name</th>
            <th>Product Name</th>
            <th>Product Price</th>
            <th>Quantity</th>
            <th>Remove from Cart</th>
          </tr>
        </thead>
        <tbody>
          {cart_product_data?.products.map((product) => {
            return (
              <tr key={product.id}>
                <td>{product.store_name}</td>
                <td>{product.product_name}</td>
                <td>${product.product_price}</td>
                <td>{product.quantity}</td>
                <td>
                  <button
                    onClick={() =>
                      removeProduct({ cart_id, product_id: product.id })
                    }
                    className="button is-light"
                  >
                    Remove Product
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <h4>Total: ${total} </h4>
      <div className="buttons">
        <button onClick={() => clearCart(cart_id)} className="button is-light">
          Clear Cart
        </button>
      </div>
      <div className="buttons">
        <button
          onClick={() => {
            editCart(data)
            checkOut(account_id)
          }}
          className="button is-light"
        >
          Check Out
        </button>
      </div>
    </>
  );
}

export default ShoppingCart;
