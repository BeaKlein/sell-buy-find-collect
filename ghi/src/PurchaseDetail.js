import { useParams } from "react-router-dom";
import { useGetCartProductsQuery } from "./store/api";


function PurchaseDetail() {
  const { cart_id } = useParams();
  const { data: cart_product_data } = useGetCartProductsQuery(cart_id);

  let total = 0;
  if (cart_product_data?.products) {
    for (let product of cart_product_data?.products) {
      total += product.product_price * product.quantity;
    }
  };

  return (
    <>
      <h1>Order ID: {cart_id}</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Store Name</th>
            <th>Product Name</th>
            <th>Product Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          {cart_product_data?.products.map((product) => {
            return (
              <tr key={product.id}>
                <td>{product.store_name}</td>
                <td>{product.product_name}</td>
                <td>${product.product_price}</td>
                <td>{product.quantity}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <h4>Total: ${total} </h4>
    </>
  );
}

export default PurchaseDetail;
