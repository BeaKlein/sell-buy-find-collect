import React, { useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';
import { useGetCartQuery, useGetTokenQuery, useLogOutMutation } from './store/api';
import { AiOutlineShoppingCart } from 'react-icons/ai';


function LogOut() {
    const [logOut, result] = useLogOutMutation();
    const navigate = useNavigate();
    useEffect(() => {
    if (result.isSuccess) {
        navigate("/");
    }
    });
    return (
        <div className="buttons">
            <button onClick={logOut} className="button is-light">
                Log out
            </button>
        </div>
    )
};


function Nav() {
    const { data: token, isLoading: tokenLoading } = useGetTokenQuery();
    const cart_id = token?.account?.cart_id;
    const { data: cartData } = useGetCartQuery(cart_id);

    return (
      <nav className="navbar navbar-expand-lg navbar-dark">
        <div className="container-fluid">
          <div className="left">
            <img
              src="https://i.imgur.com/EoKreQT.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
              alt="SBFC logo"
            />
            <Link className="navbar-brand pe-10" to="/">
              SBFC
            </Link>
          </div>
          <div className="d-flex flex-row-reverse">
            <div>
              {tokenLoading ? (
                ""
              ) : token ? (
                <LogOut />
              ) : (
                <Link className="text-white p-2" to="/login">
                  Log In
                </Link>
              )}
            </div>
            <div>
              {token?.account?.store_id != null ? (
                <Link className="text-white p-2" to="/products/new">
                  Add A Product
                </Link>
              ) : (
                ""
              )}
            </div>
            <div>
              {token?.account?.store_id === null && token ? (
                <Link className="text-white p-2" to="/stores/new">
                  Create Store
                </Link>
              ) : (
                ""
              )}
            </div>
            <div>
              {tokenLoading ? (
                ""
              ) : token ? (
                <Link
                  className="text-white p-2"
                  to={`accounts/${token?.account?.id}`}
                >
                  Profile
                </Link>
              ) : (
                <Link className="text-white p-2" to="/signup">
                  Sign Up
                </Link>
              )}
            </div>
            <div>
              {token?.account?.cart_id != null ? (
                <Link
                  className="text-white"
                  to={`cart/${token?.account?.cart_id}`}
                >
                  <AiOutlineShoppingCart size={25} />({cartData?.cart_products?.length})
                </Link>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </nav>
    );
}


export default Nav;
