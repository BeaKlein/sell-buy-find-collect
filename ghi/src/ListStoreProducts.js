import { useParams, Link } from "react-router-dom";
import { useGetStoreProductsQuery, useGetStoreQuery, useGetTokenQuery } from "./store/api";


function ListStoreProducts() {
  const { store_id } = useParams();
  const { data } = useGetStoreProductsQuery(store_id);
  const { data: store_data } = useGetStoreQuery(store_id);
  const { data: tokenData } = useGetTokenQuery();

const linkStyle = {
  color: "black"
};

  return (
    <>
      <h1 className="title text-center">{store_data?.store_name}</h1>
      <div className="text-center italic">~~~~~</div>
      <div className="text-right">
        {tokenData?.account?.store_id === store_id ? (
          <button className="mb-4 bg-white rounded">
            <Link to={`edit`}>Edit My Store</Link>
          </button>
        ) : (
          ""
        )}
      </div>
        <div className="spacing text-center">
        <div className="container">
          <div className="row">
          {data?.products.map( product => {
            return (
                <div className="col-sm-4 shadow-lg p-3 mb-3 bg-white rounded" key={product.id}>
                  <div className="mb-3" style={linkStyle}><Link to={`/products/${product.id}`}>{product.product_name}</Link></div>
                  <div><img src={product.product_img_url} className="img-float rounded" alt="product" width="300" height="300"/></div>
                  <div>${product.product_price}</div>
                </div>
            );
          })}
        </div>
        </div>
      </div>
    </>
  );
}

export default ListStoreProducts;
