import { useParams, Link } from "react-router-dom";
import { useGetTokenQuery, useGetUserQuery } from "./store/api";


function UserProfile() {
  const { account_id } = useParams();
  const { data } = useGetUserQuery(account_id);
  const { data: tokenData } = useGetTokenQuery();

  return (
    <>
      <h1>My Profile</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Link to Store</th>
            <th>Link to Sales History</th>
            <th>Link to Cart</th>
            <th>Link to Purchase History</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{data?.username}</td>
            <td>{data?.email}</td>
            <td>
              <Link to={`../../stores/${data?.store_id}`}>My Store</Link>
            </td>
            <td>{data?.sales_history}</td>
            <td>
              <Link to={`../../cart/${data?.cart_id}/`}>My Cart</Link>
            </td>
            <td>
              <Link to={`purchases/`}>Purchase History</Link>
            </td>
          </tr>
        </tbody>
      </table>
      <div>
        {tokenData?.account?.id === account_id ? (
          <button>
            <Link to={`/accounts/${account_id}/edit`}>Edit My Profile</Link>
          </button>
        ) : (
          ""
        )}
      </div>
      <div>
        {tokenData?.account?.store_id === data?.store_id ? (
          <button>
            <Link to={`../../stores/${data?.store_id}/edit`}>Edit My Store</Link>
          </button>
        ) : (
          ""
        )}
      </div>
    </>
  );
}

export default UserProfile;
