import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useLogInMutation } from './store/api';


function LoginForm() {
  const navigate = useNavigate();
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [logIn, result] = useLogInMutation();

  const handleUserNameChange = (event) => {
    const value = event.target.value;
    setUserName(value);
  };

  const handlePasswordChange = (event) => {
    const value = event.target.value;
    setPassword(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    logIn({ username, password });
  };

  useEffect(() => {
    if (result.isSuccess) {
        navigate("/");
    }
  });

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
        <div className="box content">
          <h3>Log In</h3>
          <form method="POST" onSubmit={handleSubmit} id="login-form">
            <div className="field">
              <label className="label" htmlFor="username">Username</label>
              <div className="control">
                <input required onChange={handleUserNameChange} value={username} name="username" className="input" type="text" placeholder="Username" id="username" />
              </div>
            </div>
            <div className="field">
              <label className="label">Password</label>
              <div className="control">
                <input required onChange={handlePasswordChange} value={password} name="password" className="input" type="password" placeholder="Password" id="password" />
              </div>
            </div>
            <div className="field is-grouped">
              <div className="control">
                <button className="button is-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      </div>
    </div>
  );
}

export default LoginForm;
