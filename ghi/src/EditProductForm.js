import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useGetProductQuery, useEditProductMutation } from "./store/api";


function EditProductForm() {
  const { product_id } = useParams();
  const {data: product_data} = useGetProductQuery(product_id);
  const [product_name, setProductName] = useState("");
  const [product_description, setProductDescription] = useState("");
  const [product_img_url, setProductImgUrl] = useState("");
  const [product_price, setProductPrice] = useState("");
  const [editProduct, result] = useEditProductMutation();

  const fetchData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/api/products/${product_id}`;
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setProductName(data.product_name);
      setProductDescription(data.product_description);
      setProductImgUrl(data.product_img_url);
      setProductPrice(data.product_price);
    }
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (result.isSuccess) {
      navigate(`/stores/${product_data.store_id}`);
    }
  });

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line
  }, []);


  const handleProductNameChange = (event) => {
    const value = event.target.value;
    setProductName(value);
  };

  const handleProductDescriptionChange = (event) => {
    const value = event.target.value;
    setProductDescription(value);
  };

  const handleProductImgUrlChange = (event) => {
    const value = event.target.value;
    setProductImgUrl(value);
  };

  const handleProductPriceChange = (event) => {
    const value = event.target.value;
    setProductPrice(value);
  };


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      data: {
      product_name,
      product_description,
      product_img_url,
      product_price,
    },
      product_id,
    }
    editProduct(data);
  };


  if (result.isSuccess) {
      console.log("Product update successful!");
  };


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Edit Your Product</h1>
          <form onSubmit={handleSubmit} id="update-product-form">
            <div className="form-floating mb-3">
              <input
                value={product_name}
                onChange={handleProductNameChange}
                placeholder="Product Name"
                required
                type="text"
                id="product_name"
                className="form-control"
              />
              <label htmlFor="product_name">Product Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={product_description}
                onChange={handleProductDescriptionChange}
                placeholder="Product Description"
                required
                type="text"
                id="product_description"
                className="form-control"
              />
              <label htmlFor="product_description">Product Description</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={product_img_url}
                onChange={handleProductImgUrlChange}
                placeholder="Product Image Url"
                type="url"
                id="product_img_url"
                className="form-control"
              />
              <label htmlFor="product_img_url">Product Image Url</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={product_price}
                onChange={handleProductPriceChange}
                placeholder="Product Price"
                type="number"
                id="product_price"
                className="form-control"
              />
              <label htmlFor="product_price">Product Price</label>
            </div>
            <button className="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditProductForm;
