APIS / Data Models

Product

-Method: POST, GET, GET ALL, PUT, DELETE
-Paths: /products, /products/{id}

Input:

{
    "product_name": string,
    "product_description": string,
    "product_img_url": string,
    "product_price": float,
    "store_id": string
}

Output:

{
    "id": string
    "product_name": string,
    "product_description": string,
    "product_img_url": string,
    "product_price": float,
    "store_id": string
}

Creating a new products saves the name, description, image url, product price, store id, and product id. This adds a new product to the database which can be added to the cart and purchased.

Store

--Method: POST, GET, GET ALL, PUT, DELETE
-Paths: /stores, /stores/{id}, /stores/{store_id}/add_product/{product_id},
    /stores/{store_id}/remove_product/{product_id, /{store_id}/products}

Input:

{
    "store_name": string,
    "description": string,
    "img_url": string,
    "account_id": string,
    "products": list = [],
}

Output:

{
    "store_name": string,
    "description": string,
    "img_url": string,
    "account_id": string,
    "products": list,
    "id": string,
}

Creating a new store saves the name, description, image url, account_id, an empty products list, and a store id. The personalized store, once products are added, will display the list of products related to the specific store.

Cart

--Method: POST, GET, GET ALL, PUT, DELETE
-Paths: /cart, /carts, /cart/{id}, /cart/{cart_id}/add_product/{product_id}, /cart/{cart_id}/remove_product/{product_id}, /cart/{cart_id}/clear_cart, /cart/{cart_id}/products, /api/accounts/{account_id}/remove_cart, /api/accounts/{account_id}/checkout_cart

Input:

{
    cart_products: list,
    total_cost: int,
    account_id: string
}

Output:

{
    cart_products: list,
    total_cost: int,
    account_id: string,
    cart_id: string
}

Clicking "Add to cart" on a product detail page will create a new cart id, create a new cart, and add that product to the cart. The cart lists the product details of the item, quantity, and price. You are able to remove specific items from the cart, clear the cart, and checkout your items.

Accounts
--Method: POST, GET, GET ALL, PUT, DELETE
-Paths: /api/accounts, /api/accounts/{id}, /token, /api/accounts/{account_id}/add_store/{store_id}, /api/accounts/{account_id}/remove_store, /api/accounts/{account_id}/assign_cart/{cart_id}

Input:

{
    "username": string,
    "password": string,
    "email": string,
    "store_id": Optional[string],
    "cart_id": Optional[str],
    "purchases": Optional[list],
}

Output:

{
    "username": string,
    "password": string,
    "email": string,
    "store_id": Optional[string],
    "cart_id": Optional[str],
    "purchases": Optional[list],
    "id": string,
}

Creating an account gives the user the functionality to checkout products, edit their user profile, create their own store, and add products to their store. Being the top level of authorization, a store owner, the user will have the most functionality throughout the website.
